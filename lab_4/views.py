from django.http import response
from django.shortcuts import redirect, render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all()
    return render(request, 'lab4_note_list.html', {'notes':notes})

def add_note(request):
    response = {}
    form = NoteForm(request.POST or None, request.FILES or None)
    response['form']= form
    if (request.method=='POST')&(form.is_valid()):
        # check if form data is valid
        form.save()
        return redirect(index)
        # return index(request)
        # Redirect to a success page.
    else:
        return render (request,'lab4_form.html',response)


def note_list(request):
    Notes = Note.objects.all()
    response={}
    response["Notes"]=Notes
    return render(request, 'lab2.html',response)

def tutorial_css(request):
    return render(request,'tutorial.html')

  