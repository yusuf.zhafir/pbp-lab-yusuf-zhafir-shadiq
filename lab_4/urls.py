from django.urls import path
from .views import index, add_note,tutorial_css

#path tambahan untuk friend
urlpatterns = [
    path('',index,name='index'),
    path('add-note/',add_note,name='add note'),
    path('tutorial/',tutorial_css,name='tutorial css')
    # path('add-friend/',add_friend,name='add_friend')
]

