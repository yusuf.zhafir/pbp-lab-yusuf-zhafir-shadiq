from django.urls import path
from .views import index,xml,json

#path tambahan untuk friend
urlpatterns = [
    path('',index,name='Note'),
    path('xml/',xml, name='Note XML'),
    path('json/',json,name='Note json')
]

