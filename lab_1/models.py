from django.db import models

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    DOB = models.DateField()

    def __str__(self) -> str:
        return self.name