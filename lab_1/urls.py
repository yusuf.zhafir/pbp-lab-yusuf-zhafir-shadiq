from django.urls import path
from .views import index,friend_list

#path tambahan untuk friend
urlpatterns = [
    path('', index, name='index'),
    path('friends',friend_list,name='friend_list')
]

