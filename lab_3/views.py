from django.http import response
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
# Create your views here
# .
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}
    form = FriendForm(request.POST or None, request.FILES or None)
    response['form']= form
    if (request.method=='POST')&(form.is_valid()):
        # check if form data is valid
        form.save()
        return index(request)
        # Redirect to a success page.
    else:
        return render (request,'lab3_index.html',response)
  
    # response['Today']= max_date
