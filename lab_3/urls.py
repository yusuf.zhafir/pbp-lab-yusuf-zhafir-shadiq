from django.urls import path
from .views import index, add_friend

#path tambahan untuk friend
urlpatterns = [
    path('',index,name='index'),
    path('add-friend/',add_friend,name='add_friend')
]

