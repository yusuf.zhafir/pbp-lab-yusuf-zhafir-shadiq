import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/ayo_vaksin.dart';
import 'package:provider/provider.dart';

import './dummy_data.dart';
import './screens/tabs_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/filters_screen.dart';
import './screens/categories_screen.dart';
import './screens/ayo_vaksin.dart';
import './models/meal.dart';
import './provider/theme_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };
  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;

      _availableMeals = DUMMY_MEALS.where((meal) {
        if (_filters['gluten'] && !meal.isGlutenFree) {
          return false;
        }
        if (_filters['lactose'] && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_filters['vegetarian'] && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String mealId) {
    final existingIndex =
        _favoriteMeals.indexWhere((meal) => meal.id == mealId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteMeals.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteMeals.add(
          DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoriteMeals.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => ThemeProvider(),
        builder: (context, _MyAppState) {
          final themeProvider = Provider.of<ThemeProvider>(context);

          return MaterialApp(
            title: 'Makanan sodik',
            themeMode: themeProvider.themeMode,
            theme: MyThemes.lightTheme,
            darkTheme: MyThemes.darkTheme,
            initialRoute: '/', // default is '/'
            routes: {
              '/': (ctx) => TabsScreen(_favoriteMeals),
              CategoryMealsScreen.routeName: (ctx) =>
                  CategoryMealsScreen(_availableMeals),
              MealDetailScreen.routeName: (ctx) =>
                  MealDetailScreen(_toggleFavorite, _isMealFavorite),
              FiltersScreen.routeName: (ctx) =>
                  FiltersScreen(_filters, _setFilters),
              AyoVaksin.routeName: (ctx) => AyoVaksin(),
            },
            onGenerateRoute: (settings) {
              return settings.arguments;
            },
            onUnknownRoute: (settings) {
              return MaterialPageRoute(
                builder: (ctx) => CategoriesScreen(),
              );
            },
          );
        },
      );
}
